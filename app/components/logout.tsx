// app/components/logout.tsx

import axios from "axios";
import { useRouter } from "next/navigation";

export default function LogoutButton() {
  const router = useRouter();

  const logout = async () => {
    const res = await axios.post("/api/logout");
    console.log(res.data);

    if (res.data.ok) {
      router.push("/accounts/login");
    } else {
      alert("Log out failed!");
    }
  };

  return <button onClick={logout}>Log Out</button>;
}
