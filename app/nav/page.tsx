"use client";

import React, { useState } from "react";
import LogoutButton from "../components/logout";

export default function Nav() {
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  return (
    <header>
      <nav
        className="flex items-center bg-blue-300 justify-between p-6 lg:px-8 h-20 border border-t-0 border-l-0 border-b-gray-600"
        aria-label="Global"
      >
        <div className="flex lg:flex-1">
          <a href="/" className="m-1.5 p-1.5">
            Home Page
          </a>
          <a href="/user_management" className="m-1.5 p-1.5">
            Users Management
          </a>
          <a href="/files" className="m-1.5 p-1.5">
            Files List
          </a>
        </div>
        <div className="relative inline-block text-left">
          <div>
            <button
              type="button"
              onClick={toggleDropdown}
              className="m-1.5 p-1.5"
            >
              Menu
            </button>
          </div>
          {isOpen && (
            <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
              <div
                className="py-1"
                role="menu"
                aria-orientation="vertical"
                aria-labelledby="options-menu"
              >
                <a
                  href="profile"
                  className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                  role="menuitem"
                >
                  Profile
                </a>
                <a
                  className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                  role="menuitem"
                >
                  <LogoutButton />
                </a>
              </div>
            </div>
          )}
        </div>
      </nav>
    </header>
  );
}
